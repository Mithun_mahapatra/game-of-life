variable "region" {
  type = string
  default = "ap-south-1"
}
variable "instance_type" {
  type = string
  default = "t2.micro"
}
variable "ami" {
  type = string
  default = "ami-6t6vvcrdchcjh667"
}